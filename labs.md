---
title:
- Laboratórios - Introdução ao Linux Embarcado
author:
- Cleberson Lael
- Lucas Fernandes
---

\pagebreak

# Parte I - Introdução à Linha de Comando

## Lab 1 - navegação

Ao iniciar o terminal, você será apresentado ao **prompt**. Ele possui
duas funções principais, receber comandos e apresentar informações.
A forma padrão do **prompt** em nosso sistema será:

```
[aluno@curso ~]$
```

Aqui já é possível ver algumas informações:

- O nome de nosso usuário (`aluno`);
- O nome da máquina (`curso`);
- Uma representação de nosso diretório atual (`~`).

Podemos ver que nosso diretório atual, isso é, o local em que estamos no
sistema, está representado por '`~`'. Esse caractere simboliza o local `home`
do usuário, o local que este recebe para guardar seus arquivos pessoais.

Vamos fazer uma verificação através do comando `pwd`, que nos mostra o
diretório atual, de qual é o caminho para a nossa pasta `home`.

*Digite os comandos conforme indicados após o caractere `$`*

```
$ pwd
```

Já sabemos como descobrir onde estamos, agora veremos como verificar os
conteúdos do diretório. O comando `ls` é usado para listar os conteúdos de
diretório.

```
$ ls
```

O comando `ls`, assim como a maioria dos comandos, aceita opções que modificam
a ação que ele realiza. Vamos experimentar algumas dessas opções, atente-se
nas diferenças quando cada uma das opções é utilizada.

```
$ ls -a
$ ls -l
$ ls -al
```

A opção '`-l`' (long list) exibe os conteúdos em formato de lista com detalhes,
enquanto que '`-a`' (all) exibe arquivos ocultos[^1]. As opções podem ser
combinadas em um único argumento, como mostrado em '`-al`'.

[^1]: Em sistemas Unix arquivos ocultos são aqueles que possuem os nomes
iniciados pelo caratere '`.`'.

Agora que já podemos listar conteúdos, precisamos ser capazes de acessar outros
locais do sistema. O comando `cd` nos permite entrar em outros diretórios. São
aceitos por esse comando tanto caminhos relativos quanto caminhos absolutos.

Caminhos relativos são os nomes em relação ao local que você já está. Um
exemplo, a partir de nossa pasta `home`, são os nomes de pastas que podemos ver
ao usar `ls`. Os caminhos absolutos são os caminhos completos que levam a um
local do sistema, um exemplo pode ser visto ao utilizar o comando `pwd`.
Caminhos absolutos sempre começam com o caractere '`/`', que representa
a *raíz* (root) do sistema.

```
$ ls
$ cd Documentos
$ ls -a
$ pwd
$ cd .
$ pwd
$ cd ..
$ pwd
$ cd /
$ ls
$ pwd
$ cd ~
$ pwd
```

Já somos capazes de se mover pelo sistema, porém, ainda não conseguimos mover
arquivos por ele. Para isso nosso comando de destaque será o `mv` (move).

Para apoio da atividade também utilizaremos o comando `touch`. Em essência,
esse comando altera a informação de quando um arquivo foi acessado[^2]. Apesar
disso, quando usado com um nome de arquivo inexistente, `touch` cria um
arquivo vazio com o nome indicado.

[^2]: A informação de quando um arquivo foi acessado pela última vez
(*timestamp*) pode ser vista na saída do comando '`ls -l`'.

```
$ touch arquivo1 arquivo2 arquivo3
$ ls
$ touch .oculto1 .oculto2
$ ls
$ ls -a
$ mv arquivo1 arquivo2 arquivo3 Documentos
$ ls
$ ls Documentos
$ mv .oculto1 estavaoculto1
$ mv .oculto2 estavaoculto2
$ ls
$ ls -a
$ mv estavaoculto1 estavaoculto2 Documentos
$ ls
$ ls Documentos
```

Para terminar essa atividade, vamos explorar um pouco como de é feita a criação
de novos diretórios com `mkdir` (make directory) e a remoção de arquivos com
`rm` (remove).

```
$ cd Documentos
$ ls
$ mkdir pasta1
$ ls -l
$ mv estavaoculto1 pasta1
$ ls
$ ls pasta1
$ mv arq* pasta1
$ rm estavaoculto2
$ ls
$ rm pasta1
$ rm -r pasta1
```

Observe que para remover diretórios é necessário acompanhar o comando `rm` da
opção '`-r`' (recursive). Isso indica que conteúdos que estejam no interior da
pasta também serão removidos.

**!!!** Tenha muita atenção ao utilizar o comando `rm`, pois arquivos removidos
por ele não podem ser recuperados. Diferentemente de deletar um arquivo por
meio de uma interface gráfica, os arquivos são apenas apagados, não movidos
para uma lixeira.

Uma dica é utilizar o comando `ls` para primeiro listar os arquivos indesejados
e então substituí-lo pelo comando `rm`.

\pagebreak

## Lab 2 - arquivos de texto

O objetivo desse capítulo será identificar diferentes tipos de arquivo e ser
capaz de manipular arquivos de texto plano, um dos mais importantes em um
sistema do tipo Unix.

Nos ambientes derivados do MS-DOS, tipos de arquivos costumam ser inferidos
exclusivamente por sua extensão. Por exemplo, é fácil reconhecer um arquivo
terminado em '`.exe`' como um executável e um terminado em '`.jpg`' como uma
imagem no formato JPEG.

Em ambientes tipo Unix, embora programas possam se utilizar de extensões como
forma de determinar tipos de arquivo, e muitos realmente o façam, o sistema em
si se possui outras formas de inferir o tipo de um arquivo. Normalmente através
de testes ou por dados contidos no cabeçalho do arquivo.

Como usuários do sistema, existem algumas ferramentas que podemos utilizar para
inferir facilmente sobre tipos de arquivos.

Para realizar a identificação desses tipos, existem duas formas principais. Uma
delas, mais simples, já utilizamos. Através do comando `ls` acompanhado da
opção '`-l`', o primeiro caractere de cada linha da saída indica o tipo básico
do arquivo.

Existem diversos tipos que podem ser representados dessa forma, já vimos dois
deles:

- '`-`': arquivo comum
- '`d`': diretório

Existe um outro comando capaz de mostrar bem mais detalhes sobre um tipo de
arquivo, que são obtidos através de diversos testes (ou através de mágica, ou
melhor, testes com os chamados padrões mágicos). O comando ao qual me refiro
é chamado de `file`.

A uso do comando `file` é bem simples, bastando indicar o nome do arquivo
desejado.

```
$ cd
$ ls
$ file Curso
```

Vamos checar alguns arquivos interessantes:
```
$ cd Curso/kernel
$ ls
$ file linux.tbz
$ cd boot
$ ls
$ file *
```

Perceba que no último exemplo, utilizamos o caractere '`*`', esse é um
caractere curinga. Ao encontralo, a linha de comando (o bash) realiza uma
expansão. Nesse caso, isso significa aplicar algo a todos os arquivos da pasta.

Agora, vamos trabalhar com arquivos de texto. Primeiro vamos criar um arquivo
simples:

```
$ cd
$ echo "Meu novo arquivo" > teste
$ ls
$ file teste
```

No passo anterior criamos um arquivo de texto chamado `teste`. Para visualizar
os conteúdos, vamos começar com o comando `cat` (concatenate).

```
$ cat teste
```

O comando `cat` simplesmente ecoa qualquer conteúdo de texto para a saída. Para
entendermos melhor, vamos executá-lo sem parâmetros.

```
$ cat
```

Digite uma linha qualquer e pressione enter. A linha será repitida e o comando
voltará a aguardar uma entrada. Para encerrar pressione `Ctrl`+`c`.

`cat` também pode juntar na saída conteúdos de arquivos diferentes, como seu
nome sugere.

```
$ echo "meu mais novo arquivo" > teste2
$ ls
$ cat teste teste2
```

Uma outra forma de exibir arquivos de texto é através de paginadores. Esse tipo
de programa mostra os conteúdos em uma exibição separada do resto do conteúdo
do terminal. O primeiro que veremos é o `more`. A utilização é semelhante ao `cat`.

```
$ more teste teste2
```

`more` exibe os arquivos por páginas, pressione a barra de espaço para
prosseguir. Para vermos a real utilidade de um paginador vamos precisar de mais
texto, por isso, vamos criar um novo arquivo.

```
$ ls -l /etc > muitotexto
$ more muitotexto
```

A divisão em páginas do `more` facilita um pouco a visualização. Mas ainda há
um problema, `more` só permite avançar nas páginas. Por isso foi criado um novo
programa chamado de `less` que permite a visualização para frente e para trás
no documento.

```
$ less muitotexto
```

Para sair da visualização do `less` pressione '`q`'.

O programa `less` também possui outros recursos, como busca de texto. Para
procurar por um texto no documento aberto pressione '`/`' e digite o texto
desejado, seguido de enter. Para avançar ou retornar nas ocorrências do termo,
utilize as teclas '`n`' e '`N`', respectivamente.

Para a edição de texto vamos usar um programa simples chamado `nano`. Para
abrir um documento no nano apenas use o nome do arquivo como argumento. Se
o arquivo não existir, um novo será criado.

```
$ nano hello
```

Edite o texto normalmente e quando quizer sair pressione `Ctrl`+`x`, em seguida
responda com `s` ou `n` para salvar ou não as alterações realizadas.


\pagebreak

## Lab 3 - sistema de arquivos
Nesta atividade vamos conhecer um pouco da organização dos arquivos em um
sistema Linux. Para maiores detalhes sobre como os diretórios estão organizados
consulte a página `hier` do manual (`$ man hier`).

Entre no diretório **root** e liste seus conteúdos:

```
cd /
ls
pwd
```
Cheque o diretório **/bin** para visualizar os binários essenciais do sistema:

```
$ cd /bin
$ ls
$ ls -l cp
$ file cp
$ ls -l apropos
$ file apropos
$ ls -l file
$ file file
```
Entre no diretório **/boot** para visualizar os arquivos utilizados na
utilização do sistema:

```
$ cd /boot
$ ls
```

Há alguns arquivos interessantes aqui, vamos verificar alguns deles com mais
detalhes:

```
$ ls -l vmlinuz*
```

Esses são arquivos do kernel Linux. Cheque-os com `file`:

```
$ file vmlinuz*
```

Existem outras coisas que podemos checar aqui, uma delas é a pasta `grub2`.
Essa pasta contém o bootloader[^3] do nosso sistema, o componente responsável
por carregar o kernel quando a máquina inicia. Para checar os conteúdos dessa
pasta precisaremos de permissões especiais, para isso usaremos nosso comando
através do `sudo`[^4] :

[^3]: Falaremos mais sobre bootloaders na segunda parte do curso.
[^4]: Abordaremos com mais detalhes o comando `sudo` no laboratório 5.

```
$ sudo ls -l grub2
```

Agora vamos ao diretório **/dev**, que contém os dispositivos montados à
máquina:

```
$ cd /dev
$ ls -l
```

Repare na primeira coluna da listagem do `ls`, há algumas letras novas aqui.
Dispositivos de caractere, isso é, aqueles que só podem ser acessados como um
fluxo de caracteres, são representados pelo caractere '`c`'. Alguns exemplos são
as `tty`[^5] (teletype), os diversos terminais virtuais do sistema.

[^5]: A própria interface gráfica roda em cima de um terminal virtual. Para
acessar os outros terminais virtuais em uma máquina comum, a combinação padrão
costuma ser CTR+ALT+FX (onde X é o número do `tty`). Porém, sendo esta uma
máquina virtual, a combinação é dada por HOST+FX (a tecla padrão para HOST
é o CTRL direito).\
Se desejar, tente acessar alguns dos outros `tty`s e em seguida retorne
à interface gráfica com HOST+F1.

```
$ ls -l tty*
$ file tty1
```

Outra letra nova é '`b`', que indica dispositivos de blocos. Esses são, em geral,
dispositivos de armazenamento, como HDs. Eles também podem ser vistos com
`lsblk`, que indica seus pontos de montagem, quando existentes.

```
$ lsblk
$ ls -l sda
$ file sda
```

Como em outras partes do sistema, aqui também existem links[^6]. A entrada
e saída padrão do sistema, estão representadas aqui dessa forma:

[^6]: Veremos como pode ser feita a criação de links no laboratório 4.

```
$ ls -l stdin stdout
```

Agora veremos os conteúdos do diretório **/proc**, um pseudo-diretório que
é gerenciado pelo kernel. Os conteúdos presentes nesse diretório apresentam
informações do kernel sobre o sistema.

```
$ cd /proc
$ ls
```

Perceba a existência de diversas pastas numeradas. Essas pastas são criadas
para cada processo em execução no sistema e são nomeadas conforme o **pid**
(**P**rocess **ID**) do processo.

```
$ ls -d {0..10000} 2> /dev/null
```

Não se preocupe muito com os detalhes do comando anterior, apenas realizamos
uma expansão para fornecer os números de 0 a 10000 como argumentos para
o comando `ls` e redirecionamos as mensagens de erro (decorrentes de diretórios
não existentes) para um dispositivo especial (`/dev/null`) que aceita
e descarta entradas.

Nesse diretório ainda, podemos obter algumas informações sobre a máquina. Vamos
checar os dados da CPU contidos em `cpuinfo` e os dados da memória RAM em
`meminfo`:

```
$ less cpuinfo
$ less meminfo
```

\pagebreak

## Lab 4 - criação de links

Nesta atividade veremos um pouco de como funcionam os links.

Existem dois tipos de links, **hard links** (ou links reais) e **links
simbólicos**. O primeiro tipo, já estamos utilizando a algum tempo. Hard links
são os próprios nomes de arquivos. Criar um novo hard link significa criar
outro nome para um mesmo arquivo.

Como um arquivo pode ter mais de um nome? Um arquivo é representado no
armazenamento por um número chamado **inode**. Esses números identificam um
arquivo em um dispositivo de armazenamento (HDD, pendrive, ...).

Para ver o **inode** de um arquivo acrescente a opção '`-i`' ao comando `ls -l`.

```
$ cd
$ ls -li
```

Um hard link é um nome que aponta para um inode, simples assim. Para criar um
novo link utiliza-se o comando `ln`. O primeiro argumento deve ser o arquivo
para o qual se deseja criar um novo link e o segundo argumento o novo nome de
arquivo.

```
$ touch hi
$ ls -li hi
$ ln hi hou
$ ls -li hi hou
```

Existem alguns poréns quanto ao uso de novos hard links.
- Só podem ser criados para arquivos que estejam em um mesmo dispositivo de
  armazenamento;
- Não é possível criar hard links para diretórios;
- Para remover um arquivo do disco, é necessário apagar todos os seus hard
  links.

Devido a tais limitações, um outro tipo de link foi criado, os links
simbólicos. Um link simbólico não aponta para um inode, mas sim para um
caminho. Assim é possível referenciar qualquer tipo de arquivo na árvore do
sistema através de um link simbólico.

```
$ ls
$ ln Documentos docs
$ ln -s Documentos docs
$ ls -ldi Documentos docs
```

\pagebreak

## Lab 5 - permissões e arquivos executáveis

Para a realização de algumas tarefas administrativas no sistema, são
necessárias algumas permissões adicionais que um usuário comum não possui.

A conta que possui todos os direitos em um sistema Unix é chamada de `root`. Em
geral quando se deseja demonstrar que uma tarefa requer direitos adicionais em
uma listagem, trocasse o caracter indicativo do prompt '`$`' por '`#`'.

Ficar o tempo inteiro logado como `root` pode ser perigoso, pois há um risco
real de se danificar o sistema caso se altere algum arquivo essencial. Um
exemplo disso é o comando `rm`, lembre-se que ele nem sequer pede por
confirmação para deletar os arquivos. Isso poderia causar situações
desastrosas.

Pensando-se nisso é altamente recomendada a utilização do comando `sudo` para
a realização de tarefas que requeiram privilégios. O comando `sudo` permite que
um usuário comum (não `root`) que esteja registrado como administrador do
sistema, realize um único comando como se fosse o `root`.

Vamos ver um exemplo:

```
$ sudo touch arquivodoroot
$ ls -l
```

Perceba que para cada arquivo listado aparecem dois nomes. A maioria está
listada como "aluno aluno". Porém, o novo arquivo que foi criado é listado como
"root root".

Os dois nomes refletem respectivamente o usuário e o grupo ao qual o arquivo
pertence. Existem alguns bits que refletem as autorizações de uso de cada
arquivo. Essas autorizações estão separadas em usuário, grupo e outros. Outros
aqui se refere a qualquer usuário que não seja o dono do arquivo nem faça parte
do grupo ao qual o arquivo pertence.

Para encontrar os grupos de que o usuário faz parte utilize o comando `id`.

```
$ id
$ sudo id
```
As autorizações do arquivo ou **modo de acesso** é definido pelos
9 caracteres que aparecem após o tipo de arquivo na listagem longa. Eles estão
definidos conforme três conjuntos (usuário, grupo, outros) e cada conjunto
possui 3 bits (rwx).
- `r` (read)
- `w` (write)
- `x` (execute)
A ausência de um dos bits corresponde a aquela permissão não estar definida.

Ex:
```
rwx------ # todas as permissões (leitura, escrita, execução) para o úsuario
          # grupo e outros não possuem permissão de acesso.

rw-r----- # usuário tem permissão de leitura e escrita, grupo tem permissão de
          # leitura, outros não possuem permissão
```

Tente alterar o arquivo sem utilizar os privilégios de superusuário:

```
$ nano arquivodoroot
```

É possivel alterar o usuário e o grupo que detém o arquivo através do comando
`chown` (change owner). Utilize como argumento o usuário seguido do grupo
desejado com a seguinte sintaxe:

```
chown usuario:grupo arquivo
```

Vamos alterar nosso arquivo para que ele se torne do usuário e grupo `aluno`.

```
$ sudo chown aluno:aluno arquivodoroot
$ ls -l arquivodoroot
```

Para alterar permissões do modo de acesso do arquivo utilize  a letra
correspondente a usuário (`u`), grupo (`g`) ou outros (`o`), acompanhe do sinal
de adicionar (`+`) ou remover (`-`) e em seguida os bits correspondentes as
permissões (`r`, `w`, `x`).

Vamos adicionar permissão de execução para o usuário ao nosso arquivo:

```
$ chmod u+x arquivodoroot
$ ls -l arquivodoroot
```

Há uma outra forma de especificar as permissões para o `chmod`, pode-se
utilizar o valor numérico dos bits.

```
r -> 4
w -> 2
x -> 1
```

Somando os valores dos bits que se deseja ativar é obtido um valor entre 0 e 7.
Assim, o modo de acesso do arquivo pode ser definido como 3 digitos. Vamos
utilizar essa forma para dar permissão de leitura e escrita para o usuário
e nenhuma permissão para o grupo ou outros.

```
$ chmod 700 arquivodoroot
```

\pagebreak

## Lab 6 - compressão de arquivos

Por vezes é necessario organizar arquivos em uma certa disposição de diretórios
como um arquivo único, para isso a ferramenta `tar` é o padrão em sistemas do
tipo Unix.

A forma que o `tar` recebe argumentos é um pouco diferente da maioria dos
comandos. O modo em que a ferramenta deve operar é definido apenas pelos
caracteres das opções, sem o '`-`' antecede-los, e deve ser sempre a primeira
opção. Os modos mais importantes são os seguintes:

- `c`: Criar um arquivo;
- `x`: Extrair um arquivo;
- `t`: Listar os conteúdos de um arquivo.

Outra opção importante é `f`, que especifica um arquivo alvo. Vamos criar um
novo arquivo com `tar`:

```
$ cd
$ mkdir -p pasta/pasta{01..10}
$ ls
$ ls pasta
$ tar cf pasta.tar pasta
$ ls
$ file pasta
$ tar tf pasta.tar
```

Embora tenhamos juntado tudo em um arquivo só (arquivamento), nenhuma
compressão foi realizada. A ferramenta `tar` apenas agrupa os arquivos, sem
realizar compressão (diferente da ferramenta `zip`, possivelmente mais comum
aos leitores, que realiza arquivamento e compressão).

Para realizar compressão é comum utilizar em conjunto com o `tar` a ferramenta
`gzip`, que faz uma compressão mais rápida porém menos otimizada, ou `bzip2`,
que comprime mais ao custo de um tempo maior.

As ferramentas `gzip` e `tar` podem ser usadas separadamente, mas como nosso
objetivo é comprimir o arquivo gerado pelo `tar`, podemos utilizar as opções
deste último que chamam as ferramentas de compressão.

- `z`: utiliza o gzip;
- `j`: utiliza o bzip2.

Dessa vez, vamos criar um arquivo comprimido:

```
$ tar czf pasta.tgz pasta
$ file pasta.tgz
$ tar tf pasta.tgz
$ ls
$ rm -r pasta
$ tar xvf pasta.tgz
$ ls
```

A opção `v` (verbose) mostrada no último exemplo exibe na tela o que está sendo
feito pelo programa. É comum usar as extensões `.tgz` e `tbz` ao invés de
`.tar.gz` e `.tar.bz`.

\pagebreak

## Lab 7 - scripts e binários

Agora que já somos capazes de criar aquivos de texto e alterar permissões de
arquivos podemos prosseguir para a criação de programas executáveis. Veremos
dois tipos de programas, aqueles que são interpretados a partir do próprio
código fonte (scripts), e binários executáveis, que são criados ao compilar um
código fonte (por exemplo em C).

Começaremos pelos scripts, para isso vamos ver exemplos em duas linguagens
comuns: python e bash[^7]. A seguir, vamos criar um novo arquivo:

[^7]: A linguagem bash é a mesma que é utilizada pela linha de comando do
Linux. Todos os comandos que executamos na linha de comando até
aqui estão, na verdade, sendo executados pelo interpretador do bash.

```
$ cd
$ nano hello.py
```

Insira o seguinte texto no conteúdo do arquivo e salve:

```
print("Hello world")
```

Essa é uma encarnação de um dos programas mais simples que você vai encontrar
(o famoso "hello world"). Ele simplesmente imprime "`Hello world`" na tela.

Como rodar nosso programa? Vamos chamar o interpretador do python para isso:

```
$ python3 hello.py
```

Se o texto for exibido corretamente nosso programa está ok. O próximo passo
é tornar nosso programa um executável por si mesmo.

```
$ chmod +x hello.py
```

Vamos tentar executá-lo:

```
$ ./hello.py
```

O programa não funciona corretamente, o que falta? Simples, agora que estamos
executando o arquivo diretamente o sistema não sabe que o interpretador precisa
ser usado. É necessário sinalizar qual interpretador deve ser usado. Vamos
checar onde está nosso interpretador com o comando `which`, que vai indicar
o endereço de um executável:

```
$ which python3
```

Agora adicionaremos essa informação ao programa. O local do interpretador deve
ser indicado na **primeira linha** do programa, após a sequência especial de
caracteres '`#!`' (shebang, derivado de "hash" e "bang", nomes comumente usados
para esses caracteres).

```
$ nano hello.py
```

Inserir no arquivo:

```
#!/usr/bin/python3
```

Salve o arquivo e tente executar novamente:

```
$ ./hello.py
```

Perfeito! Agora pense sobre uma coisa, por que nosso programa precisa ter
o local indicado na chamada (colocando o '`./`') e o `python3` não? Isso ocorre
porque quando digitamos um comando o sistema procura por ele em alguns
diretórios específicos. Esses diretórios estão definidos em uma variável do
ambiente chamada `PATH`. Quais são os conteúdos de `PATH`?

```
echo $PATH
```

Em `PATH`, os diretórios são separados por '`:`'. Poderiamos alterar a variável
e adicionar nosso diretório a ela[^8], porém, isso não é necessário. Veja
o último diretório listado na variável (`~/bin`), ele fica em nossa home. Esse
local costuma vir predefinido em muitas distribuições por ser um local comum
para colocar scripts do usuário. Vamos mover nosso comando para lá
e renomeá-lo, retirando o `.py` que já não é necessário:

[^8]: Para alterar a variável `PATH` apenas dariamos um novo valor a ela da
seguinte forma:\
`$ PATH=$PATH:/novocaminho`\
Isso manteria os dados iniciais e adicionaria um caminho novo (que precisa ser
absoluto). Para que isso fosse aplicado aos comandos executados é necessário
exportar esse valor:\
`$ export PATH=$PATH:/novocaminho`\
E para que essa configuração se tornasse permanente, ela precisaria ser
adicionada às configurações do usuário, adicionando essa linha ao arquivo
`~/.bashrc` .

```
$ mkdir ~/bin
$ mv hello.py ~/bin/hello
$ hello
```

Agora podemos executar nosso arquivo como se fosse um programa qualquer do
sistema. Em seguida faremos um exemplo um pouco mais útil. Será um programa em `bash`
que vai nos informar nosso usuário e quais scripts existem em `~/bin`:

```
$ cd ~/bin
$ nano myinfo
```

Insira esses conteúdos no arquivo:

```
#!/bin/bash

echo "Seu usuário é:"
whoami

echo
echo "Scripts do usuário"
cd ~/bin
for item in * ; do
    file "${item}"
done
```

Não poderemos nos aprofundar nos detalhes da linguagem bash nesse curso, mas
basicamente o que o script faz é executar o comando `whoami`, que informa quem
é o usuário atual, e executar o comando `file` para cada arquivo em `~/bin`.

Em seguida vamos executar nosso novo programa:

```
$ chmod +x myinfo
$ myinfo
```

Nosso próximo programa também será um "hello world", mas dessa vez ele sera
criado em C. Aproveitaremos a chance para aprender sobre compilação de
programas.

O compilador para C que utilizamos é o `gcc` (GNU Compiler Collection), que
é um projeto que foi iniciado como um compilador para C e acabou se tornando um
conjundo de compiladores para diversas linguagens.

Vamos escrever nosso programa:

```
$ cd
$ nano hellow.c
```

Insira no arquivo:

```
#include <stdio.h>

int main()
{
    puts("Hello world!");

    return 0;
}
```

Salve o arquivo.

Agora precisamos compilar. Para o `gcc` indicaremos o arquivo de entrada
e o arquivo de saída (com a opção `-o`):

```
$ gcc hellow.c -o hellow
$ ls
```

Vamos mover o arquivo e executar:

```
$ mv hellow bin/
$ hellow
```

\pagebreak

## Lab 8 - instalação de programas

Além dos programas que já vem em nossa máquina, é esperado que precisemos
intalar outros. Existem diversas formas de fazer uma instalação (incluse
através da interface gráfica, a maioria disponibiliza alguns programas bem
intuitivos para essa tarefa), vamos abordar 3 delas:

- gerenciador de pacotes;
- arquivo de pacote;
- compilação.

Utilizar um gerenciador de pacotes para obter o programa da internet é a forma
mais simples[^9]. Também é possível utilizar o gerenciador de pacotes para
resolver as dependências de um pacote local e instalá-lo. O último método
é a maneira clássica do Unix (muito mais complicada) que envolve resolver todas
as dependencias do programa manualmente, compilando o programa e pondo os
arquivos nos diretórios adequados.

[^9]: Devido ao nosso sistema de testes ser um Fedora, nossos pacotes serão
arquivos `.rpm` e o gerenciador de pacotes será o `dnf`. Em sistemas derivados
do Debian como o Ubuntu, os pacotes utilizados são `.deb` e o gerenciador
é o `apt`.\
A sintaxe do `dnf` e `apt` para busca e instalação de programas é bastante
similar.

\pagebreak

# Parte II - Introdução ao Linux Embarcado

\pagebreak

## Lab 9 - utilizando uma toolchain pré-compilada

A ferramenta mais importante para o desenvolvimento em Linux Embarcado
é a toolchain. A toolchain conterá o nosso compilador (gcc) preparado para
realizar compilação cruzada, isso é, compilar não para a arquitetura de nossa
máquina, mas sim do target desejado.

A título de exemplo vamos supor que nossa platarforma desejada (target) é um
Raspberry Pi. A primeira forma de obter uma toolchain adequada é baixando uma
pronta. Dentre alguns projetos que disponibilizam toolchains pré-compiladas
o Linaro é um dos mais conhecidos. A fabricante do Raspberry Pi disponibiliza
uma toolchain baseada no Linaro e já configurada para o Raspberry.

Os arquivos da toolchain já foram colocados na máquina de testes para que não
seja necessário baixar os arquivos. A toolchain está compactada no diretório
que contém o material do curso. Vamos descompactá-la:

```
$ cd Curso/tools/
$ ls
$ tar xjf gcc-linaro-arm.tbz
$ mv gcc-linaro-arm-linux-gnueabihf-raspbian ~/
$ cd
$ ls
```

Vamos checar os conteúdos da toolchain:

```
$ ls gcc-linaro-arm-linux-gnueabihf-raspbian/
```

Perceba que a existência dos diretórios `bin` e `lib`, que contém as os
binários do compilador e as bibliotecas para o target respectivamente.

Vamos compilar nosso velho `hellow.c` novamente, agora para o target:

```
$ ./gcc-linaro-arm-linux-gnueabihf-raspbian/bin/arm-linux-gnueabihf-gcc hellow.c -o hellow
$ ls
$ file hellow
```

De acordo com a saída do comando `file` o programa foi compilado adequadamente.
Mais à frente veremos como testar esse programa.

\pagebreak

## Lab 10 - compilando uma toolchain

A próxima coisa que desejamos é conseguir compilar nossa própria toolchain. Por
que isso é importante? Compilar a nossa própria toolchain permite que
a personalizemos conforme nossa necessidade. Em casos onde estejamos
desenvolvendo para uma nova placa ou uma que não possua um suporte tão bom
nesse quesito, ser capaz de criar a toolchain com as configurações adequadas
é essencial.

A ferramenta que iremos utilizar para nos ajudar com a compilação da toolchain
se chama Crosstool NG (Next Generation). Para nossa conveniência os arquivos
também já estão disponibilizados na máquina. Vamos começar extraindo:

```
$ cd ~/Curso/tools/
$ tar xzf crosstool-ng.tgz
$ ls
$ mv crosstool-ng ~/
$ cd
```

Primeiro é necessário gerar alguns arquivos:

```
$ cd crosstool-ng
$ ./bootstrap
```

Em seguida vamos instalar a ferramenta localmente:
*Obs: Em nossa máquina de testes todos os pacotes necessários já foram
instalados, mas caso fosse necessário configurar o crosstol para outra máquina,
antes de realizar o próximo passo, consultariámos a página no projeto
(http://crosstool-ng.github.io/docs/os-setup/) e veriamos quais são os pacotes
que precisam ser instalados em nosso sistema*

```
$ ./configure --enable-local
$ make
```

Agora vamos configurar a toolchain para o target. Para listar algumas
configurações base para alguns targets suportados execute:

```
$ ./ct-ng list-samples
```

Existe uma configuração para arm cortex a8, uma arquitetura que é
bastante utilizada, vamos tomá-la como base. Para ver mais detalhes sobre uma
dessas configurações execute o `ct-ng` e o nome da configuração, colocando
`show-` na frente:

```
$ ./ct-ng show-arm-cortex_a8-linux-gnueabi
```

Selecione o target:

```
$ ./ct-ng arm-cortex_a8-linux-gnueabi

```

Agora vamos abrir o menu para configuração do target:

```
$ ./ct-ng menuconfig
```

Será aberta uma tela de configuração, navegue pelas opções utilizando as setas
do teclado e `barra de espaço` para selecionar/deselecionar opções e `enter`
para os controles na parte de baixo da tela. Vamos alterar algumas opções:

- Em "Paths and misc options" desabilite "Render the toolchain read-only"
    - Isso irá permitir a instalação de bibliotecas adicionais a toolchain posteriormente
- Em "Target options" altere o valor de "Floating point" para "hardware (FPU)"
    - Essa opção fará com que os cálculos de ponto flutuante no target sejam
      feitos através do hardware. Em plataformas que tenham suporte, isso
      permite obter uma melhor performance do processador

Salve e saia da configuração. Um arquivo `.config` será criado contendo nossas
configurações.

```
$ less .config
```

Após dar uma olhada na configuração, saia da visualização do `less`. Agora vamos compilar a toolchain propriamente, esse passo deve levar bastante tempo:

```
$ ./ct-ng build
```

A toolchain será criada em `~/x-tools`. Vamos precisar nos referir a esse local
várias vezes, por isso vamos adicioná-lo ao nosso `PATH`.

```
$ nano ~/.bashrc
```

Adicione o seguinte ao final do arquivo:

```
export PATH=$PATH:/home/aluno/x-tools/arm-cortex_a8-linux-gnueabi/bin/
```

Para ver os efeitos da configuração feche o terminal e abra um novo ou digite:

```
$ source ~/.bashrc
```

Tente recompilar nosso `hellow.c`:

```
$ arm-cortex_a8-linux-gnueabi-gcc hellow.c -o hellowa8
$ file hellowa8
```

\pagebreak

## Lab 11 - compilando o bootloader
A primeira parte de nosso sistema que devemos compilar é o bootloader,
a ferramenta responsável por carregar o kernel do sistema. Para tal, nossa
escolha será o U-Boot.

Entre na pasta onde está o u-boot e descompacte:

```
$ cd
$ Curso/bootloader
$ ls
$ tar xjf u-boot.tbz
$ cd u-boot
```

Agora precisamos ativar alguma configuração para o u-boot. Para ver uma lista
das placas suportadas, cheque o diretório `configs`:

```
$ ls configs | less
```

Vamos escolher uma placa que possibilitará nossos testes futuros com o QEMU:

```
$ make vexpress_ca9x4_defconfig
$ make -j 2 CROSS_COMPILE=arm-cortex_a8-linux-gnueabihf-
```

Aguarde a compilação terminar. Alguns arquivos serão criados, dentre eles
usaremos o `u-boot` que contém nossa imagem do bootloader.

\pagebreak

## Lab 12 - testando o bootloader

Agora iremos testar o nosso bootloader com o programa QEMU. Esse é um programa que nos permite emular diversas máquinas, com diferentes arquiteturas. Iremos executá-lo para emular uma máquina baseada na arquitetura ARM, por isso executaremos ele como `qemu-system-arm`.

Para ver as máquinas suportadas para essa arquitetura utilize a opção `-M` (machine) acompanhada de `help`:

```
$ qemu-system-arm -M help
```

A máquina que iremos utilizar é a `vexpress-a9`:

```
$ QEMU_AUDIO_DRV=none \
qemu-system-arm -m 256M -M vexpress-a9 -no-reboot -kernel u-boot
```

A primeira linha apenas impede que apareçam mensagens de erro
relacionadas ao driver de audio (já que não possuímos um configurado).

\pagebreak

## Lab 13 - compilando o kernel

O próximo componente que iremos compilar é o mais importante de todos,
responsável por permitir a operação de nosso sistema, o kernel. As fontes do
kernel Linux podem ser obtidas em `kernel.org`. Não será necessário realizar
o download, pois os arquivos foram previamente colocados em nosso material.

Encontre e descompacte o arquivo:

```
$ cd
$ cd Curso/kernel
$ tar xjf linux.tbz
$ cd linux*
```

As arquiteturas para as quais temos configurações predefinidas estão listadas no diretório `arch`:

```
$ ls arch
```

Vamos utilizar a arquitetura `arm` com a configuração `multi_v7_defconfig`:

```
make ARCH=arm multi_v7_defconfig
```

No arquivo `.config` gerado, variáveis não "setadas" são os componentes que não serão compilados, aquelas atribuídas a valores `y` são compiladas como parte do sistema, e as atribuídas a valores `m` são compiladas como modulos.

```
$ less .config
```

Agora iremos compilar o kernel e gerar um arquivo `zImage`, que é um estágio adicional, uma imagem do kernel comprimida.

```
$ make -j 2 ARCH=arm CROSS_COMPILE=arm-cortex_a8-linux-gnueabihf- zImage
```

Também iremos compilar as **device trees**, arquivos que apresentam informações sobre o hardware. Serão geradas versões compiladas que chamamos de **device tree blobs**:

```
$ make -j 2 ARCH=arm CROSS_COMPILE=arm-cortex_a8-linux-gnueabihf- dtbs
```

Para compilar módulos que tenham sido configurados:

```
$ make -j 2 ARCH=arm CROSS_COMPILE=arm-cortex_a8-linux-gnueabihf- modules
```

Agora testaremos nosso kernel com o QEMU:

```
$ QEMU_AUDIO_DRV=none \
qemu-system-arm -m 256M -M vexpress-a9 -kernel arch/arm/boot/zImage -dtb
arch/arm/boot/dts/vexpress-v2p-ca9.dtb -append "console=tty0"
```

Nesse ponto o kernel dara um erro irrecuperável (`kernel panic`), isso ocorre
pois ainda não criamos o rootfs.

\pagebreak

## Lab 14 - gerando o rootfs

Para gerar um sistema de arquivos (rootfs) usaremos o programa `busybox`. Na
verdade, essa não é a função principal do `busybox`. Sua função é oferecer
diversas ferramentas comuns em um sistema do tipo Unix em um único binário.

Descompacte o busybox:

```
$ cd
$ cd Curso/tools
$ tar xf busybox*
```

Vamos começar a configurá-lo:

```
$ make distclean
$ make defconfig
```

Podemos fazer mais algumas configurações:

```
$ make menuconfig
```

Configure o diretório de instalação em "Busybox Settings | Installation Options
(CONFIG_PREFIX)".

Em seguida compilaremos, gerando o binário `busybox`:

```
$ make -j 2 ARCH=arm CROSS_COMPILE=arm-cortex_a8-linux-gnueabihf-
$ make install
```

Ainda precisamos copiar as bibliotecas da toolchain para o nosso sistema:

```
$ cp -avf ~/x-tools/arm-cortex_a8-linux-gnueabihf/sysroot/lib ~/rootfs/
```

Também precisamos criar mais alguns diretórios:

```
$ cd ~/rootfs
$ mkdir proc sys dev
```

<!--
\pagebreak

## Lab 15 - testando o sistema
- qemu

\pagebreak

## Lab 16 - acesso à GPIO (?)
-->
