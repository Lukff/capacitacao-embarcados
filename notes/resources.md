# Device Tree
- https://www.raspberrypi.org/documentation/configuration/device-tree.md
- https://www.devicetree.org/

# Toolchain
- https://www.linaro.org/downloads/
- http://toolchains.free-electrons.com/
- https://github.com/raspberrypi/tools/

# Bootloader
- https://sergioprado.org/como-desenvolver-um-sistema-linux-do-zero-para-a-raspberry-pi/
- https://elinux.org/RPi_U-Boot

