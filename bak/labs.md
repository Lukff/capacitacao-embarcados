---
title:
- Laboratórios - Introdução ao Linux Embarcado
author:
- Cleberson Lael
- Lucas Fernandes
---

\pagebreak

# Parte I: Introdução à Linha de Comando

## Lab 1: Navegando no Sistema de Arquivos

O objetivo desse laboratório é dar início à nossa introdução à linha de
comando do Linux. A primeira coisa que vamos abordar é como navegar no
sistema de arquivos do sistema, isso é, descobrir seu caminho atual, entrar
em diretórios (pastas), listar conteúdos, etc.


---------------------------------------------------------------------------
 Comando       Descrição                                                  
-------------- ------------------------------------------------------------
 pwd           exibe o nome do diretório atual (present working directory) 

 ls            lista os conteúdos de um diretório (list)                   

 cd            altera o diretório atual (change directory)                  

 touch         altera a marca de tempo de arquivos                  

 mv            move e renomeia arquivos (move)                  

 mkdir         cria diretórios (make directory)                  

 rm            deleta arquivos ou diretórios (remove)                  
-------------- ------------------------------------------------------------

Table: Novos comandos (Lab 1)


**Emulador de Terminal**

A maior parte das atividades que iremos realizar deverão ser feitas dentro de
um emulador de terminal (ou apenas terminal, simplificadamente).
Para obter acesso a um terminal na máquina de testes execute os passoas
descritos em uma das alternativas abaixo:

- Clique no ícone do emulador de terminal na barra de ferramentas ou no menu 
de programas, como mostrado na imagem 1;
- Use o atalho de teclado `Ctrl`+`Alt`+`t` (segure as
teclas `Ctrl` e `Alt` no teclado e pressione a tecla `t`, em seguida solte
todas as teclas).

![Abrir o emulador de terminal](img/emulador-de-terminal.png)

O resultado deverá ser visto a partir da abertura de uma nova janela, conforme
a imagem 2.

![Janela do emulador de terminal](img/open-terminal.png)

\pagebreak

**Atividade 1**

Ao iniciar o terminal, você será apresentado ao *prompt*. Ele possui
duas funções principais, receber comandos e apresentar informações.
A forma do *prompt* padrão em nosso sistema será:

```
[aluno@localhost ~]$
```

Aqui já é possível ver algumas informações:

- O nome de nosso usuário (`aluno`);
- O nome da máquina (`localhost`);
- Uma representação de nosso diretório atual (`~`).

Podemos ver que nosso diretório atual, isso é, o local em que estamos no
sistema, está representado por '`~`'. Esse caractere simboliza o local *home*
do usuário, o local que este recebe para guardar seus arquivos pessoais.

Vamos fazer uma verificação através do comando `pwd`, que nos mostra o
diretório atual, de qual é o caminho para a nossa pasta *home*.

*Digite os comandos conforme indicados após o caractere `$`*

```
$ pwd
```

Já sabemos como descobrir onde estamos, agora veremos como verificar os
conteúdos do diretório. O comando `ls` é usado para listar os conteúdos de
diretório.

```
$ ls
```

O comando `ls`, assim como a maioria dos comandos, aceita opções que modificam
a ação que ele realiza. Vamos experimentar algumas dessas opções, atente-se
nas diferenças quando cada uma das opções é utilizada.

```
$ ls -a
$ ls -l
$ ls -al
```

Agora que já podemos listar conteúdos, precisamos ser capazes de acessar
outros locais do sistema. O comando `cd` nos permite entrar em outros
diretórios.

São aceitos pelo comando tanto caminhos relativos quanto caminhos absolutos.
Caminhos relativos são os nomes em relação ao local que você já está. Um
exemplo, a partir de nossa pasta *home*, são os nomes de pastas que podemos
ver ao usar `ls`. Os caminhos absolutos são os caminhos completos que levam
à um local do sistema, um exemplo pode ser visto ao utilizar o comando `pwd`.
Caminhos absolutos sempre começam com o caractere `/`, que representa a *raíz*
do sistema.

```
$ ls
$ cd Documentos
$ ls -a
$ pwd
$ cd .
$ pwd
$ cd ..
$ pwd
$ cd /
$ ls
$ pwd
$ cd ~
$ pwd
```

Já somos capazes de se mover pelo sistema, porém, ainda não conseguimos mover
arquivos por ele. Para isso nosso comando de destaque será o `mv`.

Para apoio da atividade também utilizaremos o comando `touch`. Em essência,
esse comando altera a informação de quando um arquivo foi acessado[^1]. Apesar
disso, quando usado com um nome de arquivo inexistente, `touch` cria um
arquivo vazio com o nome indicado.

[^1]: A informação de quando um arquivo foi acessado pela última vez
(*timestamp*) pode ser vista na saída do comando '`ls -l`'.

```
$ touch arquivo1 arquivo2 arquivo3
$ ls
$ touch .oculto1 oculto2
$ ls
$ ls -a
$ mv arquivo1 arquivo2 arquivo3 Documentos
$ ls
$ ls Documentos
$ mv .oculto1 estavaoculto1
$ mv .oculto2 estavaoculto2
$ ls
$ ls -a
$ mv estavaoculto1 estavaoculto2 Documentos
$ ls
$ ls Documentos
```

Para terminar essa atividade, vamos explorar um pouco como é feita a criação de
novos diretórios com `mkdir` e a remoção de arquivos com `rm`.

```
$ cd Documentos
$ ls
$ mkdir pasta1
$ ls -l
$ mv estavaoculto1 pasta1
$ ls
$ ls pasta1
$ mv arq* pasta1
$ rm estavaoculto2
$ ls
$ rm pasta1
$ rm -r pasta1
```

## Lab 2: Um Tour pelo Sistema de Arquivos

O objetivo desse laboratório é investigarmos um pouco sobre a organização dos
dos diretórios do sistema (*filesystem hierarchy*). Vamos checar alguns dos
locais principais e tentar identificar os arquivos que encontramos.

Para uma visão mais detalhada da hierarquia do sistema de arquivos verifique
a entrada "`hier`" do manual[^2].

[^2]: Para sair de uma exibição do manual, utilize a tecla '`q`'.

```
$ man hier
```

---------------------------------------------------------------------------
 Comando       Descrição                                                  
-------------- ------------------------------------------------------------
 cat           concatena arquivos e escreve na saída padrão (concatenate)

 more          apresenta os conteúdos de um arquivo uma página por vez

 less          semelhante ao "more", porém também permite 
               movimento de retorno

 tree          lista os conteúdos de um diretório em formato de árvore
-------------- ------------------------------------------------------------

Table: Novos comandos (Lab 2)

**Atividade 2**

