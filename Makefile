all: presentation.pdf labs.pdf

presentation.pdf: presentation.md
	pandoc presentation.md -V lang=pt-BR -t beamer -o presentation.pdf

labs.pdf: labs.md
	pandoc labs.md -V lang=pt-BR -t latex -o labs.pdf
