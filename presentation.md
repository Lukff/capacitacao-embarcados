---
title:
- Introdução ao Linux Embarcado
author:
- Cleberson Lael
- Lucas Fernandes
theme:
- Frankfurt
---

# Introdução

## Motivação

- Curso de Linux Embarcado da Embedded Labs

![Curso de Linux Embarcado ministrado no Cefet/RJ](img/embedded_labs.png)

## Tópicos

- A plataforma Linux
    - Linha de Comando
- A utilização do Linux em Sistemas Embarcados

## Agenda

### Dia 1
- Sistemas Embarcados
- Sistemas Unix
- Linux
- Introdução à Linha de Comando

### Dia 2
- Linux Embarcado
- Toolchain
- Bootloader

### Dia 3
- Busybox
- Rootfs
- Kernel Linux


##O que é um Sistema Embarcado?

*"Um sistema embarcado é um dispositivo com um computador dentro que não parece
com um computador"*

Máquinas de lavar, TVs, impressoras, carros, robôs, servidores, armas,
roteadores, celulares...

Quando trabalhamos com sistemas embarcados devemos nos preocupar com dois
sistemas, o sistema host, em que você vai estar editando e compilando,
e o sistema target em que você vai efetivamente aplicá-lo.

-----

![Sistemas Embarcados](img/devices.png)

## Porque usar Linux?

Em agosto de 1991, um estudante, Linus Torvalds, de 20 anos postou a frase
mais conhecida do mundo computacional: "Hello everybody out there...I'm doing
a (free) operating system (just a hobby, won't be anything big and
professional like GNU)...it probably will never support anything other than
AT-hard disk, as that's all I have". Vários desenvolvedores através do mundo
contribuiram com o seu código. Linus escolheu o nome Linux para o sistema
operacional e como um mascote um pinguim, após ser mordido por um em um
zoológico.

-----

"I’m doing a (free) operating system (just a hobby, won’t be big and
professional like gnu) for 386(486) AT clones. This has been brewing
since april, and is starting to get ready. I’d like any feedback on
things people like/dislike in minix, as my OS resembles it somewhat
(same physical layout of the file-system (due to practical reasons)
among other things)."\
\begin{flushright}
    Linus Torvalds, 1991
\end{flushright}

-----

"Estou criando um sistema operacional (livre)(**apenas um hobby, não vai ser
grande e profissional como o gnu**) para clones AT 386(486). Está sendo preparado
desde abril, e está começando a ficar pronto. Gostaria de qualquer feedback
sobre coisas que as pessoas gostam/desgostam no minix, já que meu SO lembra
ele de certa forma (mesmo layout físico do sistema de arquivos (devido a razões
práticas) entre outras coisas)."\
\begin{flushright}
    Tradução livre
\end{flushright}

-----

Linux é um sistema operacional open source, baseado em GNU General Public
License(GNU GPL) desenvolvido pelo visionário Richard Stallman. Richad
Stallman foi o fundador da FSF(Free Software Foundation), uma associação sem
fins lucrativos que lutava pelo movimento software livre, open-source.
Stallman lutava pela liberdade de usar o sotware da maneira como quiser, a
liberdade de modificar e adequar o sofware a sua necessidade, a liberdade de
compartilhar o software com seus amigos e vizinhos, a liberdade para
compartilhar as alterações que você fez no software.

-----

Porque sua televisão usa Linux? Aparentemente a televisão teria só a função de
stream, então porque um complexo sistema tipo Unix como o Linux é necessário?
Bem o coração do sistema embarcado é a alta integração de chips com os mais
diferenciados tipos de funções. Nos referimos a ele como *"System on Chip"*, or
SoC. Sua tv não é simplesmente mostrar a stream de um video. O stream é digital
e muito possivelmente criptografado, e é necessário processamento para
a criação de uma imagem. As televisões estão ou logo estaram conenctadas
a internet, com interação com seus smartphones e tablets. Você precisa de um
completo sistema para gerenciar todos esses graus de dificuldade.

-----

Alguns pontos do porquê utilizar Linux:

- possui as necessidades fundamentais, como suportar USB, Wi-Fi, Bluetooth,
muitos tipos de armazenamentos de media, suporta bem varios tipos de
dispositivos multimidia.
- tem portabilidade para as mais diversas arquiteturas de processadores.
- Linux é open-source, possibilitando um acesso livre ao seu código fonte, te
dando o moder de modifica-lo da maneira que precisar.
- Linux tem uma comunidade bastante ativa, a cada 10 ou 12 semanas lança uma
nova atualização do seu kernel.

Por essas razões o Linux é a ideal escolha para dispositivos complexos

# Introdução à Ambientes Unix

## O Sistema Unix
- Ken Thompson no projeto do Multics
- Começo do sistema UNIX (anteriormente UNICS) em 1969
    - AT&T's Bell Labs
    - Minicomputador DEC PDP-7 (Programmable Data Processor)
    - Ken Thompson
    - Dennis Richie
- UNIX versão 4 em 1973 reescrito em C

-----

![Ken Thompson e Dennis Richie](img/Ken_n_dennis.jpg)

-----

![Minicomputador PDP-7](img/PDP-7.jpeg)

-----

- Sistema de tempo compartilhado (time-sharing)
- Teletipos e terminais
- Programas simples que executem bem uma tarefa
    - *"Keep It Simple, Stupid"*
- Programas conectados por uma interface simples

-----

![Teletipo](img/teletype.jpg)

-----

![Terminal](img/dumb-terminal.jpg)

-----

- BSD versão 1, University of California at Berkeley
    - UNIX versão 6 em 1975
- Sistemas proprietários ~~padronizados~~
    - AT&T
    - Sun Microsystems
    - IBM
    - HP
- POSIX (Portable Operating System Interface), IEEE 1003

-----

- Richard M. Stallman
- MIT's AI Lab
- GNU (GNU's Not Unix!), iniciado em 1982
- Free Software Foundation

-----

![GNU](img/gnu.png)

-----

![Richard Matthew Stallman](img/Stallman.jpeg)

-----

- HURD
- Minix, 1987
    - Andrew S. Tanenbaum
- O kernel Linux, 1991
    - Linus Torvalds

-----

![Linus Torvalds](img/torvalds.jpg)

<!-- -->

# O Sistema Linux

## Distribuições

- Distribuições (ou distros):
    - Ubuntu
    - Fedora
    - Debian
    - Arch Linux
- Interfaces Gráficas
    - Gnome
    - Xfce
    - KDE
- Gerenciamento de Pacotes
    - APT (.deb)
    - DNF (.rpm)

\footnotetext[1]{Por razões práticas usaremos a nomenclatura \textbf{Linux}
para se referir ao sistema operacional completo (o que alguns chamam de
\textbf{GNU/Linux})}

-----

![Distros](img/distros.png)

-----

![KDE Plasma](img/kde-plasma.jpg)

-----

![Gnome Shell](img/gnome-shell.jpg)

-----

![LXDE](img/lxde.png)

## Noção do que é Open Source

É open source não quer dizer que é comercializável 

- **Mito 1:** *"Linux is Free"*
    - Linux não é grátis, Linux é livre! Do 2º. parágrafo da GPL: *"When we
      speak of free software, we are refering to freedom, not price"*

- **Mito 2:** *"Não consigo proteger a propriedade intelectual do meu produto"*
    - Consegue sim, basta tomar alguns cuidados com licenças de software!

## Licenças de Software

- Software Livre
    - Free Software Foundation
- Open Source
    - Open Source Initiative

-----

![Open Source Initiative](img/osi.png)

-----

![Free Software Foundation](img/fsf.png)

-----

**Sofware Livre** (Free Software)

- As quatro liberdades:
- A liberdade de executar o programa como desejar, para qualquer propósito
  (liberdade 0).
- A liberdade de estudar como o programa funciona, e modificá-lo para que
  ele faça sua computação da forma desejada (liberdade 1). Ter acesso ao
  código fonte é uma precondição para isso.
- A liberdade de redistribuir cópias para que você possa ajudar seu vizinho
  (liberdade 2).
- A liberdade de distribuir cópias das suas versões modificadas para outros
  (liberdade 3). Fazendo isso você pode dar a toda a comunidade uma chance
  de se beneficiar de suas alterações. Acesso ao código fonte é uma
  precondição para isso.

-----

**Software Livre** (Free Software)

- Copyleft
    - Redistribuição sob os mesmos termos

- GPL (GNU General Public License)
- LGPL (GNU Lesser General Public License)
    - Bibliotecas
    - Permite linkagem com programas não livres

-----

**Open Source** (Código Aberto)

- Englobam as licenças de *free software*
- Definição originada da **Debian Free Software Guidelines**

-----

**Open Source** (Código Aberto)

1. Livre Redistribuição
2. Código Fonte
3. Trabalhos Derivados
4. Integridade do Código Fonte do Autor
5. Sem Discriminação Contra Pessoas ou Grupos

-----

**Open Source** (Código Aberto)

6.  Sem Discriminação Contra Áreas de Ação
7.  Distribuição de Licença
8.  Licença Não Deve Ser Específica à um Produto
9.  Licença Não Deve Restringir Outros Programas
10. Licença Deve Ser "Technology-Neutral"

# Introdução à Linha de Comando

## O Terminal

![Terminal](img/terminal-v2.png)


## O Prompt

- Recebe comandos do usuário
- Exibe informações
- Pode ser personalizado
<!-- -->

    ```
    [usuario@maquina diretorio]$
    $
    #
    ```

- ***Ex:***
<!-- -->

    ```
    [lukff@dungeon ~]$
    [root@dungeon /]#
    ```

## Estrutura de comandos

- Sintaxe padrão
<!-- -->

    ```
    $ comando argumentos
    ```

- Argumentos
    - Nomes de Arquivos
    - Opções
<!-- -->

        ```
        $ ls -a
        $ ls -l
        $ ls -al
        $ ls --all
        $ ls -l diretorio
        ```


k# Navegando no Sistema de Arquivos

Comandos básicos:

- Entrar em um diretório\
(`cd` - change directory)

- Ver os conteúdos de um diretório\
(`ls` - list)

- Saber o caminho atual\
(`pwd` - present working directory)

\footnotetext[1]{\textbf{Diretório} significa o mesmo que \textbf{pasta}}

## Alterando o ambiente
Mover e renomear arquivos\
(`mv` - move)

Remover arquivos\
(`rm` - remove)

Criar diretórios\
(`mkdir` - make directory)

\footnotetext[1]{Na demonstração seguinte criaremos arquivos vazios com 
                 \texttt{touch}}

## Laboratório - Navegação

![Laboratório 1](img/lab00-v3.png)

## Buscando ajuda

**Manual** (`man`)

- Informações sobre comandos
    - Utilização
    - Opções
- Dividido em seções
- Ex: "`man 1 man`" ou "`man man`"
- Procurar páginas
    - "`apropos`" ou "`man -k`"

-----

**Info** (`info`)

- Mais restrito que o `man`
- Multipágina
- Apresenta links
- Procurar
    - "`info -k`"

-----

**Opção de ajuda**

```
    -h
    --help
```

- Suportada por alguns comandos
- Descrição da opções
- Ex: "`mkdir --help`"

## Arquivos

*"Tudo é um arquivo"*

- Texto
- Binários
- Links (simbólicos)

Determinar o tipo de arquivo
(`file`)

## Arquivos de texto

- Configurações
- Códigos fonte
- Logs

## Exibição de arquivos de texto

Imprimir conteúdos\
(`cat` - concatenate)

**Paginadores**\
(`less`)\
(`more`)

*"less is more"*

## Editores de texto

- A Guerra Santa dos Editores
    - **Vim** X **Emacs**

- Um editor simples: `nano`

-----

**Vim** - "Vi Improved", anteriormente "Vi Imitation"

- Descende do clássico editor Vi do Unix
- Edição modal

## Laboratório - Trabalhando com arquivos de texto

![Laboratório](img/lab02.png)


## Sistema de Arquivos (`rootfs`)

- Organização dos arquivos no sistema
    - Árvore única
    - Dispositivos são "montados" como ramos

-----

**/** \
(root)

- Raíz do sistema de arquivos
- Tudo é montado a partir daqui

-----

**/bin** \
(binaries)

- Binários do sistema (programas executáveis)
    - Executáveis essenciais para inicialização e reparo do sistema

-----

**/boot**

- Kernel
- Arquivos necessários para a inicialização do sistema
- Configurações do bootloader

-----

**/dev** \
(devices)

- Arquivos especiais ou de dispositivos
- Pseudo-dispositivos
    - **/dev/null**
    - **/dev/zero**
    - **/dev/random**

-----

**/etc**

- Arquivos de configuração do sistema
- Shell scripts que iniciam os serviços do sistema
- Arquivos em texto plano
- **/etc/modules**
    - Módulos (drivers) carregáveis pelo sistema

-----

**/home**

- Arquivos de usuários
    - Cada usuário recebe um diretório com direitos de escrita

- A "home" do usuário `root` fica em **/root**


-----

**/lib**

- Bibliotecas do sistema

-----

**/mnt** \
**/media**

- Pontos de montagem para dispositivos removíveis
    - Pendrives
    - CD-ROMs
    - HDs externos

-----

**/opt** \
(optional)

- Usado para a instalação de alguns programas adicionais
- Softwares comerciais costumam ser instalados nesse diretório

-----

**/proc**

- Sistema de arquivos virtual (não é armazenado no HD)
- Mantido pelo kernel
- Informações do sistema

-----

**/sbin** \
(system binaries)

- Binários que executam tarefas vitais do sistema
- Restritos ao usuário `root`

-----

**/tmp** \
(temporary)

- Arquivos temporários

-----

**/var** \
(variable)

- Arquivos que estejam sujeitos a grande variação
- **/var/log**
    - Logs do sistema

-----

**/usr**

- Local padrão para arquivos de programas utilizados por usuários comuns
- **/usr/lib**
- **/usr/bin**

## Laboratório - Um Tour pelo Sistema de Arquivos

![Laboratório](img/lab01.png)

## Links

- Hard links
- Links simbólicos

- Criação de links
(`ln` - link)

-----

**Hard links**

- Permitem a um arquivo ter diversos nomes
- Um usuário não pode criar esse tipo de link para diretórios
- Restritos a um mesmo dispositivo de armazenamento

-----

**Links Simbólicos**

- Não possuem as limitações dos **hard links**
- É um arquivo diferente do original, que contém o nome deste

## Laboratório - Criação de links
![Laboratório](img/lab03.png)

## Permissões de Arquivos

- São configuradas para cada arquivo com relação a três tipos de usuários
    - Dono (Owner)
    - Grupo (Group)
    - Outros (Others)
- 3 bits configuram o acesso para cada um desses
    - `r` - read (leitura)
    - `w` - write (escrita)
    - `x` - execute (execução)

- Podem ser vistas com `ls -l`
- O "dono" e "grupo" do arquivo podem ser alterados com `chown` (change owner)
- As permissões podem ser alteradas com `chmod` (change mode)

## Scripts Executáveis

- Podem ser escritos em qualquer linguagem que possua um binário interpretador
    - Bash
    - Python
    - Awk
- O interpretador deve ser especificado após uma combinação de caracteres na
  primeira linha do arquivo
    - `#!` (shebang)
- Deve ser configurado como executável e estar no `PATH`

## Binários executáveis

- Arquivos compilados
- Para C e muitas outras linguagens o compilador GCC pode ser utilizado
    - **GCC** - "Gnu Compiler Collection", anteriormente "Gnu C Compiler"

## Laboratório - Permissões e Arquivos Executáveis

## Instalação de Programas

- Compilação dos códigos fonte
    - `make`

- Pacotes
    - `.deb`
    - `.rpm`

- Gerenciadores de Pacotes
    - `apt`
    - `dnf`
    - Permitem facilmente a procura e instalação de programas

## Laboratório - Instalando Programas

# Linux Embarcado


## Os 4 elementos de um Linux Embarcado:

- **Toolchain:** É o compilador e as outras ferramentas necessárias para
  criar o código fonte para o dispositivo target

- **Bootloader:** é necessário para inicializar o dispositivo

- **Kernel:** é o coração do sistema, gerenciando o sistema e conversando com
  o harware do seu dispositivo

- **Root filesystem:** contém as bibliotecas e programas que são lidos logo
  após o kernel ser iniciado


## Toolchain

É um grupo de ferramentas que compila o código fonte em um executável que pode 
rodar no target device

- Geralmente uma toolchain para Linux é baseada em componentes do projeto GNU
- Deve ser criada/configurada de acordo com a arquitetura da CPU do target

-----

**Uma Toolchain é dividida em 3 partes:**

- **Compilador** (GCC - GNU Compiler Collection)
    - Compatível com as linguagens C, C++, Ada, Fortran e Java, dentre
      outras.Pode gerar código para diversas arquiteturas, incluindo ARM,
      AVR, Blackfin, MIPS, PowerPC, x86, etc.

- **Assembler e Linker** (Binutils)
    - Binutils é um conjunto de ferramentas para manipular arquivos binários
      para uma arquitetura específica.

- **Bibliotecas padrão do C** (Standard Libraries)
    - A toolchain depende da biblioteca C, já que ela irá linká-la com sua
      aplicação para gerar os binários para a arquitetura-alvo.

-----

![Bibliotecas](img/library.png)

-----

**Tipos de Toolchain:**

- **Toolchain nativa:** essa toolchain roda no mesmo tipo de sistema, as vezes
  no mesmo sistema que o programa gerador. É utilizado geralmente para desktops
  e servidores.

- **Toolchain Cross** (Cross-Compiling Toolchain): essa toolchain um *host* com
  aquitetura diferente da do *target*

-----

![Tipos de Toolchain](img/toolchain-types.png)

-----

**Uma forma alternativa e mais confortável:**

- CrossTool-NG (next generation)
    - é uma boa forma de você compilar a sua toolchain pois permite
      que você veja todo o processo da sua criação


##Bootloader

O Bootloader é responsável basicamente pela inicialização do sistema e o
carregamento do kernel.

O Bootloader segue uma série de execução de códigos e verificações que
chamamos de **estágios**

-----

**1ºEstágio - Código ROM**

O código que roda imediatamente após o reset ou o power-on tem que ser
armazenado em um SoC (Sistem on Chip), esse é o código ROM. Ele é programado no
chip em sua manofatura, sendo assim o código ROM não é e não pode ser
substituido por um equivalente open souce.

-----

**2ºEstágio - SPL**

Ativa o controlador de memória e outras partes essenciais do sistema para
carregar o third stage program loader(TPL) automaticamente sem a intervenção
do usuário.

-----

**3º Estágio - TPL**

Agora, finalmente, estamos executando um bootloader completo como U-Boot ou
Barebox. Geralmente, existe uma interface de usuário de linha de comando
simples que permitirá que você execute tarefas de manutenção, como carregar
novas imagens de inicialização e kernel em armazenamento flash, carregar e
inicializar um kernel, e há uma maneira de carregar o kernel automaticamente
sem a intervenção do usuário.
No final da terceira fazer o kernel está na memória só aguardando a sua
inicialização. Uma vez que o kernel está sendo executado o bootload desaparece
a memória.

-----

**Escolha do Bootloader:**

![Bootloaders](img/bootloader_table.png)

-----

**U-Boot**

U-Boot ou Das U-Boot é um bootloader que oferece suporte para muitos tipos de
arquiteturas além de ter uma comunidade ativa trabalhando com ele.


## Kernel
O Kernel é basicamente o coração do sistema embarcado, ele atua na comunicação
do hardware com o *user space*

O User Space é composto basicamente por Aplicações e a Biblioteca de C

O kernel roda em modo privilegiado, com total acesso à todas as instruções da
CPU, endereçamento de memória e I/O, enquanto que os processos do usuário rodam
em modo restrito, com acesso limitado aos recursos da máquina.

-----

![Kernel](img/kernel.png)

-----

![Kernel Space](img/kernel-space.png)

